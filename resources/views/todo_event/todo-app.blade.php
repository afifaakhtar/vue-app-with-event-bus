
@extends('layouts/fullLayoutMaster')

@section('title', 'Manage Tasks')

@section('vendor-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.2.7/css/rowReorder.dataTables.min.css"/>

@endsection

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
  {{--  <link rel="stylesheet" href="{{ asset(mix('css/pages/app-user.css')) }}">--}}

@endsection

@section('content')

  @if ($errors->any())
    @foreach ($errors->all() as $error)
      <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert">&times;</a>&nbsp
        <strong>Error!</strong> {{ $error }}
      </div>
    @endforeach
  @endif
  @if(Session::has('success'))
    <div class="alert alert-success">
      <a href="#" class="close" data-dismiss="alert">&times;</a>&nbsp
      <strong>Success!</strong> {{Session::get('success')}}
    </div>
  @endif
  <!-- Course create start -->
  <section class="users-edit">
    <div class="card">
      <div class="card-content">
        <div class="card-body">
          <div id="app">
            <add-task></add-task>
          </div>
          <!-- users edit account form start -->
{{--            <div class="row">--}}
{{--              <div class="col-12 col-sm-6">--}}
{{--                <h5 class="mb-1"><i class="feather icon-plus-square mr-25"></i>Add New Task</h5>--}}
{{--                <br>--}}
{{--              </div>--}}
{{--            </div>--}}
{{--            <div class="row">--}}
{{--              <div class="col-6">--}}
{{--                <div class="form-group">--}}
{{--                  <div class="controls">--}}
{{--                    <label>Title <span class="text-danger">*</span></label>--}}
{{--                    <input type="text" name="title" class="form-control  required" placeholder="Task Title"--}}
{{--                           data-validation-required-message="Task title is required">--}}
{{--                  </div>--}}

{{--                </div>--}}
{{--              </div>--}}
{{--              <div class="col-6">--}}
{{--                <div class="form-group">--}}
{{--                  <label>Date</label>--}}
{{--                    <input type='text' class="form-control pickadate" />--}}
{{--                </div>--}}
{{--              </div>--}}
{{--            </div>--}}
{{--            <div class="divider divider-right">--}}
{{--              <div class="divider-text"> <button type="button" class="btn btn-md btn-clean btn-outline-primary" >Add Task</button></div>--}}
{{--            </div>--}}

            {{--Add Lesson Modal --}}
            <div class="modal fade" id="addLesson" tabindex="-1" role="dialog"
                 aria-labelledby="addLessonTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
                   role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="addLessonTitle">Add Lesson</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body p-2">
                    <div class="form-group row">
                      <div class="col-md-4">
                        <span>Select Lesson</span>
                      </div>
                      <div class="col-md-8">
                        <select id="lesson-select" class="form-control">
                          <option selected hidden value="">Select Lesson</option>
                              <option value="">Title</option>


                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">

                    <button type="button" class="btn btn-success" onclick="addLesson(document.getElementById('lesson-select').options[document.getElementById('lesson-select').selectedIndex]);"><i class="feather icon-plus-circle mr-25"></i>Add</button>
                  </div>
                </div>
              </div>
            </div>
            {{---Add Lesson Model End---}}
            <div id="app1">
              <show-incomplete-task></show-incomplete-task>
            </div>
          <br>
          <hr>
          <br>
          <div id="app2">
            <show-complete-task></show-complete-task>
          </div>




        </div>
      </div>
    </div>
  </section>
  <!-- users edit ends -->

@endsection
@section('vendor-script')
  {{-- Vendor js files --}}
  {{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jqBootstrapValidation.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script>
  <script src="{{ asset(mix('js/scripts/pages/app-user.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/datatables/datatable.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/navs/navs.js')) }}"></script>
  <script>
      var count = 1;
      var contentTable = $('#content-table').DataTable({
          //for reordering of rows
          // rowReorder: true,
          // //FIXME::Table is not responsive
          // responsive: true,
          // language: {
          //     emptyTable:     "No lesson found"
          // },
          // columnDefs: [ {
          //     "searchable": false,
          //     "orderable": false,
          //     "targets": 0
          // } ],

      });
      contentTable.on( 'order.dt search.dt', function () {
          contentTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              cell.innerHTML = i+1;
          } );
      } ).draw();

  </script>
@endsection
